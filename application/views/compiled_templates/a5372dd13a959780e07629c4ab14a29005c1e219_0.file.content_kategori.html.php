<?php
/* Smarty version 3.1.30, created on 2017-05-20 13:45:04
  from "/Applications/MAMP/htdocs/web/application/views/templates/public/content_kategori.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_591fe5f04159f3_97548095',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a5372dd13a959780e07629c4ab14a29005c1e219' => 
    array (
      0 => '/Applications/MAMP/htdocs/web/application/views/templates/public/content_kategori.html',
      1 => 1495245973,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_591fe5f04159f3_97548095 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="zui section bg-light">
		<div class="inner">	
			<form class="ads-general-filter">
				<div class="row">
					<div class="col-sm-6">
						<span class="ads-gf-sorting">
							<span class="ads-gf-label">Sort by:</span>
							<select name="sort">
								<option>Date</option>
								<option>Sales</option>
								<option>Rating</option>
								<option>Price</option>
							</select>
						</span>
						<span class="ads-gf-control separate"><i class="fa fa-sort-amount-asc"></i></span>
						<span class="ads-gf-control active"><i class="fa fa-th"></i></span>
						<span class="ads-gf-control"><i class="fa fa-th-list"></i></span>
					</div>
					<div class="col-sm-6 ads-gf-text-right">
						<span class="ads-gf-label">Ads per page:</span>
						<span class="ads-gf-control active">15</span>
						<span class="ads-gf-control">30</span>
						<span class="ads-gf-control">60</span>
						<span class="ads-gf-control">90</span>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="zui section space-50">
		<div class="inner">
					
	
			<div class="row row-md-3-columns row-xs-2-columns">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'prod');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->value) {
?>
				
				<div class="col">
					<div class="ad-style-grid">
						<a href="#" class="photo">
							<img src="<?php echo assets_url();?>
/uploads/images/<?php echo $_smarty_tpl->tpl_vars['prod']->value->foto;?>
" alt="" />
							<div class="meta">
								<span class="price">Rp <?php echo $_smarty_tpl->tpl_vars['prod']->value->harga;?>
</span>
							</div>
						</a>
						<h3 class="title"><a href="#"><?php echo $_smarty_tpl->tpl_vars['prod']->value->judul;?>
</a></h3>
						<div class="category subheading">from <a href="#"><?php echo $_smarty_tpl->tpl_vars['prod']->value->id_member->nama;?>
</a></div>
					</div>
				</div>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</div>
			<div class="load-more-ads">
				<a href="#">Load more</a>
			</div>

		</div>
	</div><?php }
}
