<?php
/* Smarty version 3.1.30, created on 2017-05-20 13:45:00
  from "/Applications/MAMP/htdocs/web/application/views/templates/public/content_detail.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_591fe5ec0a66b3_59872602',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82d3c4da2f4aedc9f68cf5fabed0a30f8e85ab4d' => 
    array (
      0 => '/Applications/MAMP/htdocs/web/application/views/templates/public/content_detail.html',
      1 => 1495245560,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_591fe5ec0a66b3_59872602 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="zui section space-50">
		<div class="inner">
			<div class="content-with-sidebar">

				<div class="the-content">

					<div class="the-ad-single">
						<div class="the-ad-single-title">
							<h1><?php echo $_smarty_tpl->tpl_vars['detail']->value->judul;?>
</h1>
							<div class="subheading">
								<span><a href="#" class="the-ad-author"><?php echo $_smarty_tpl->tpl_vars['detail']->value->id_member->nama;?>
</a></span>
								<span><i class="icon fa fa-calendar"></i> <?php echo $_smarty_tpl->tpl_vars['detail']->value->created_at;?>
</span>
							</div>
						</div>

						<div class="the-ad-photo"><img src="<?php echo assets_url();?>
uploads/images/<?php echo $_smarty_tpl->tpl_vars['detail']->value->foto;?>
" alt=""  style="width:100%" /></div>

						<div class="the-ad-content">
							<div class="row">
								<div class="col-md-3 col-sm-6">
									<div class="sum">
										<div class="number price">Rp <?php echo $_smarty_tpl->tpl_vars['detail']->value->harga;?>
</div>
									</div>
								</div>
							</div>
							<div class="specs-section">
								<div class="the-ad-description">
									<h4>Description</h4>
									<p><?php echo $_smarty_tpl->tpl_vars['detail']->value->deskripsi;?>
</p>
				
								</div> <!-- .ad-description -->
							</div>
						</div>
					</div> <!-- .ad-single -->

					<div class="contact-seller">
						<h3>Contact the seller</h3>
						<div class="row">
							<div class="col-sm-5">
								<div class="zui form-label">Your name</div>
								<input type="text" name="name" class="fullwidth" />
							</div>
							<div class="col-sm-7">
								<div class="zui form-label">Your email</div>
								<input type="text" name="email" class="fullwidth" />
							</div>
							<div class="col-sm-12">
								<div class="zui form-label">Message</div>
								<textarea class="normal"></textarea>
								<div class="zui form-description">Ask the seller everything you what to know about this ad.</div>
							</div>
							<div class="col-12">
								<br><input type="submit" name="submit" value="Send message" />
							</div>
						</div>
					</div>

				</div> <!-- .the-content -->

				<div class="the-sidebar">
					<div class="widget">
						<h3 class="widget-title">Category</h3>
						<div class="widget-content">
							<ul class="widget-category">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'cat');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->value) {
?>
								<li><a href="<?php echo base_url();?>
iklan/kategori/<?php echo $_smarty_tpl->tpl_vars['cat']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value->nama;?>
</a></li>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>
				
							</ul>
						</div>
					</div>

					<div class="widget">
						<h3 class="widget-title">Recently viewed</h3>
						<div class="widget-content">
							<div class="widget-ads-list">
								
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['related']->value, 'prod');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->value) {
?>
									<div class="w-ad-item">
										<div class="row smallgutter row-sm-middle">
											<div class="col-xs-3">
												<a href="#"><img src="<?php echo assets_url();?>
uploads/images/<?php echo $_smarty_tpl->tpl_vars['prod']->value->foto;?>
" alt="" /></a>
											</div>
											<div class="col-xs-9">
												<a href="#" class="title"><?php echo $_smarty_tpl->tpl_vars['prod']->value->judul;?>
</a>
												<span class="zui mark red">Rp <?php echo $_smarty_tpl->tpl_vars['prod']->value->harga;?>
</span>
											</div>
										</div>
									</div>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


							</div>
						</div>
					</div>
					
				</div> <!-- .the-sidebar -->

			</div> <!-- .content-with-sidebar -->

		</div>
	</div><?php }
}
